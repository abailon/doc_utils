# Documentation utils
This provides a set of tools to use to build documentations
or presentations. The goal is to have something generic that
could be reused for most of our projects.

# How it works
There is a collection of Makefiles and some other useful file
that could be used (theme, pictures, etc) to build a presentation
or a documentation.
The Makefile must be included by a Makefile, or used directly.
It expects some environment variable to be defined.
The Makefile will use them to create the output directory,
figure out which document to build, and which files to use for that.
In addition, using mustache, this will replace some variables
defined in the documentation files by value defined in a yaml
file. This give the ability to build different documentations
from the same files.

# Setup
## Markdown only
To use this tool, you will need to install three tools:
- `pandoc`, which is used to build the documentation
- `pandoc-mustache`, which is used to replace variable in documentation
- `yaml-merge`, which is to merge the variable files during the doc generation

The upstream version of `pandoc-mustache` can't be used
(it doesn't support variable substitution in code blocks),
so we must install our fork using this command:
```
pip install https://github.com/BayLibre/pandoc-mustache/archive/master.zip --user
```
`pandoc` could be installed using the package manager:
```
apt-get install pandoc
```
`yaml-merge` could be installed via `npm`:
```
npm install -g @alexlafroscia/yaml-merge
```
More information: https://github.com/alexlafroscia/yaml-merge

## PDF
If we want to build a documentation in pdf format then,
we must install much more tools.
Pandoc use latex as backed to generate the pdf, so we must install it:
```
apt-get install texlive
```

# How to use it
Basically, you just need to fetch this repo in your documentation
sources, and run or include the Makefile you wish to use.
Before to include the Makefile, there are few variables to define:
- `PDF`: This variable is optional. If set to true, this will build a pdf file.
- `PROJECT_PATH`: The path where to find the project files
- `PROJECT`: The base name of the project files.
  Basically, the makefile will try to include a file named
  $(PROJECT).mk and use $(PROJECT).yaml to substitute the
  variables in the final document.
  The following variable should be defined in the $(PROJECT).mk
  file.
- `DOCS_PATH`: The folder where to find the documentation files
  that will be used to generate the documentation
- `DOCS`: The list of document to build
- `*_FILES`: Should define the list of file to include to build
  a document. As example define `README_FILES` to define the files
  to use to build README.md (or README.pdf).
  The file name should given without extension,
  and the path is relative to `DOCS_PATH`.
```
$ make -f doc_utils/md.mk PROJECT=test PROJECT_PATH=path/to/project
```
