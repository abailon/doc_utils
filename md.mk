# Determine the absolute path of doc_utils (that is, the directory of this
# makefile)
# This is required to include some files from it
MD_MK_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
DOC_UTILS_PATH := $(CURDIR)/$(notdir $(patsubst %/,%,$(dir $(MD_MK_PATH))))

# Check if all the required variables are defined
ifeq ($(PROJECT_PATH),)
$(warning "PROJECT_PATH is not defined, using $(CURDIR)")
PROJECT_PATH := $(CURDIR)
endif

ifeq ($(PROJECT),)
$(error "PROJECT is not defined")
endif

ifeq ($(PDF),true)
EXT := pdf
else
EXT := md
endif

# Include the project file that defines all the important variables
include $(PROJECT_PATH)/$(PROJECT).mk

OUT := $(CURDIR)/out/$(PROJECT)
OUT_FILE := $(OUT)/$(DOC_NAME)

# Check if all the required variables are defined
ifeq ($(DOCS_PATH),)
$(warning "DOCS_PATH is not defined, using $(CURDIR)")
DOCS_PATH := $(CURDIR)
endif

ifeq ($(DOCS),)
ifeq ($(DOC_NAME),)
$(error "You must define DOCS or DOC_NAME")
endif
endif

ifneq ($(DOC_NAME),)
ifeq ($($(DOC_NAME)_FILES),)
$(error "No documents to build, please define $(DOC_NAME)_FILES")
endif
endif

# Try to find all the useful yaml files to merge them
# The final yaml file $(OUT)/variables.yaml should also contains
# all the expected variables. If a variable is empty, then, a yaml file
# is missing or incomplete.
YAML_FILES := $(addprefix $(DOCS_PATH)/,$(addsuffix .yaml,general $(DOCS)))
YAML_DOCS_FILES := $(wildcard $(DOCS_PATH)/*.yaml)
YAML_FILES := $(filter $(YAML_DOCS_FILES),$(YAML_FILES))
YAML_FILES += $(addprefix yaml/,$(addsuffix .yaml,$(YAML_PROJECT_FILES)))
ifneq ("$(wildcard $(PROJECT_PATH)/$(PROJECT).yaml)","")
YAML_FILES += $(wildcard $(PROJECT_PATH)/$(PROJECT).yaml)
endif

# Generate virtual targets that could be used to build one document
DOC_TARGETS := $(addprefix $(OUT)/,$(DOCS))

# Covert the list of document file to build to a list of absolute file path
$(DOC_NAME)_FILES := $(addprefix $(DOCS_PATH)/,$(addsuffix .md,$($(DOC_NAME)_FILES)))
$(DOC_NAME)_FILES := $(DOC_UTILS_PATH)/header.md $($(DOC_NAME)_FILES)

all: $(DOC_TARGETS)

$(OUT)/%:
	make -f $(DOC_UTILS_PATH)/md.mk PROJECT_PATH=$(PROJECT_PATH) \
		PROJECT=$(PROJECT) DOC_NAME=$* $(OUT)/$*.$(EXT)

$(OUT):
	@mkdir -p $@

$(OUT)/variables.yaml: $(OUT) $(YAML_FILES)
	@yaml-merge $(YAML_FILES) > $@

$(OUT_FILE).md: $(OUT)/variables.yaml $($(DOC_NAME)_FILES)
	@cd $(OUT); pandoc --filter pandoc-mustache --toc $($(DOC_NAME)_FILES) \
		-f markdown+lists_without_preceding_blankline -t markdown \
		-o $(DOC_NAME).md

$(OUT_FILE).pdf: $(OUT_FILE).md
	@pandoc --toc $(OUT_FILE).md -o $(OUT_FILE).pdf

clean:
	@rm -r -f $(OUT)

mrproper:
	@rm -r -f out
